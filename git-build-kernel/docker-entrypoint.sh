#!/bin/sh

set -e

# From docker environment:
#GBK_USER
#GBK_AUTOBUILD_DIR
#GBK_GIT_REFERENCE_DIR
#GBK_CHROOTS_DIR
#GBK_OUT_DIR

GBK_HOME=$(getent passwd ${GBK_USER} | cut -d: -f6)

# --hook=<pre-receive|post-receive>
GBK_HOOK=pre-receive

while [ $# -gt 0 ]; do
  case "$1" in
  "--hook"|--hook=*)
    if [ -n "${1#*=}" ] && [ "${1#*=}" != "$1" ]; then
      GBK_HOOK=${1#*=}
    elif [ $# -ge 2 ]; then
      GBK_HOOK=$2; shift
    fi
    ;;
  "--help")
    echo "Usage: $0 [--hook=<pre-receive|post-receive>]"; exit 0 ;;
  *)
    echo "Unknown argument '$1'. Exit." >&2; exit 1 ;;
  esac
  shift
done

case "${GBK_HOOK}" in
pre-receive|post-receive) ;;
*)
  echo "Unknown usage '${GBK_HOOK}'. Exit." >&2; exit 1 ;;
esac

print_fingerprints() {
  basedir=${1-'/etc/ssh'}
  for item in dsa rsa ecdsa ed25519; do
    [ -f "${basedir}/ssh_host_${item}_key" ] || continue;

    echo ">>> Fingerprints for ${item} host key"
    ssh-keygen -E md5 -lf "${basedir}/ssh_host_${item}_key"
    ssh-keygen -E sha256 -lf "${basedir}/ssh_host_${item}_key"
    ssh-keygen -E sha512 -lf "${basedir}/ssh_host_${item}_key"
  done
}

# Generate Host keys, if required
if ls /etc/ssh/keys/ssh_host_* 1> /dev/null 2>&1; then
  echo ">> Found host keys in '/etc/ssh/keys'"
elif ls /etc/ssh/ssh_host_* 1> /dev/null 2>&1; then
  echo ">> Found host keys in '/etc/ssh'"
  mkdir -p /etc/ssh/keys
  cp /etc/ssh/ssh_host_* /etc/ssh/keys/
else
  echo ">> Generating new host keys in '/etc/ssh/keys'"
  mkdir -p /etc/ssh/keys
  ssh-keygen -A
  mv /etc/ssh/ssh_host_* /etc/ssh/keys/
fi
print_fingerprints /etc/ssh/keys

# Fine tune sshd_config
sed -i -e 's,^#\?Port\s.*$,Port 22,' \
       -e 's,^#\?PermitRootLogin\s.*$,PermitRootLogin no,' \
       -e 's,^#\?PasswordAuthentication\s.*$,PasswordAuthentication no,' \
       -e 's,^#\?HostKey\s\+/etc/ssh/\(.*\)$,HostKey /etc/ssh/keys/\1,' \
       -e 's,^#\?UsePAM\s.*$,UsePAM no,' \
       -e 's,^#\?PermitTTY\s.*$,PermitTTY no,' \
       -e 's,^#\?X11Forwarding\s.*$,X11Forwarding no,' \
       -e 's,^#\?PrintMotd\s.*$,PrintMotd no,' \
       -e 's,^#\?Subsystem\s\+\(.*\)$,#Subsystem \1,' \
    /etc/ssh/sshd_config

echo "ForceCommand /usr/local/bin/gbk-force-command.sh" >> /etc/ssh/sshd_config

# Create git bare repository if empty.
if ! git --git-dir="${GBK_AUTOBUILD_DIR}" config --list --local >/dev/null 2>&1; then
  mkdir -p "${GBK_AUTOBUILD_DIR}"
  rm -rf "${GBK_AUTOBUILD_DIR:?}"/* || true

  echo ">> Creating ${GBK_AUTOBUILD_DIR}"
  git init --bare "${GBK_AUTOBUILD_DIR}"

  if git --git-dir="${GBK_GIT_REFERENCE_DIR}" config --list --local >/dev/null 2>&1; then
    echo "${GBK_GIT_REFERENCE_DIR}/objects" > "${GBK_AUTOBUILD_DIR}/objects/info/alternates"
  fi
else
  echo ">> Found valid ${GBK_AUTOBUILD_DIR}"
fi

ADDITIONAL_GITCONFIGS="
  receive.advertisePushOptions=true
  gbk.output=${GBK_OUT_DIR}
"
for conf in ${ADDITIONAL_GITCONFIGS}; do
  git --git-dir="${GBK_AUTOBUILD_DIR}" config "${conf%%=*}" "${conf#*=}";
done

echo ">> Using hook '${GBK_HOOK}'"

ln -sf "${KTEAMTOOLS_DIR}/git-build-kernel/${GBK_HOOK}" \
    "${GBK_AUTOBUILD_DIR}/hooks/${GBK_HOOK}"

chown -R "${GBK_USER}.${GBK_USER}" "${GBK_AUTOBUILD_DIR}"

mkdir --mode=0700 "${GBK_HOME}/.ssh"
cp /etc/ssh/keys/authorized_keys "${GBK_HOME}/.ssh/authorized_keys"
chown -R "${GBK_USER}.${GBK_USER}" "${GBK_HOME}/.ssh"

echo ">> Using output directory '${GBK_OUT_DIR}'"

chown git.sbuild "${GBK_OUT_DIR}"
chmod 770 "${GBK_OUT_DIR}"
chmod g+s "${GBK_OUT_DIR}"

if ! grep "^${GBK_OUT_DIR}" /etc/schroot/default/fstab; then
  echo "${GBK_OUT_DIR} ${GBK_OUT_DIR} none rw,rbind 0 0" \
      >> /etc/schroot/default/fstab
fi

# Fix "Missing privilege separation directory: /run/sshd"
mkdir -p /run/sshd

exec /usr/local/bin/chroot-entrypoint.sh /usr/sbin/sshd -D -e -f /etc/ssh/sshd_config
