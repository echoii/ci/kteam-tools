#!/bin/sh

set -e

# Copy available schroot config
for chroot in "${KTEAMTOOLS_CHROOTS_DIR}"/*; do
  flavor=${chroot##*/}
  [ -f "${chroot}/${flavor}" ] || continue

  echo ">> Adding schroot config '${flavor}'"
  cp "${chroot}/${flavor}" /etc/schroot/chroot.d
done

exec "$@"
