variables:
  CHROOT_SETUP_STAGING_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-chroots-"
  CHROOT_SETUP_TAG_PREFIX: "${CI_REGISTRY_IMAGE}/chroots:"
  CHROOT_SETUP_DEFAULT_SERIES: "jammy"

.chroot-setup-build-base: &chroot-setup-build-base
  stage: prerequisites
  image: docker:git
  services:
    - docker:dind
  needs: []
  before_script:
    - echo "${CI_REGISTRY_PASSWORD}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
  script:
    - |
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export TAG="${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_SERIES}";
    - if [ -n "${CI_MERGE_REQUEST_IID}" ]; then
        export KTEAMTOOLS_URL=${CI_MERGE_REQUEST_PROJECT_URL};
        export KTEAMTOOLS_FETCH=${CI_MERGE_REQUEST_REF_PATH};
      else
        export KTEAMTOOLS_URL=${CI_PROJECT_URL};
        if [ "${CI_COMMIT_REF_NAME}" != "${CI_DEFAULT_BRANCH}" ]; then
          export KTEAMTOOLS_BRANCH=${CI_COMMIT_REF_NAME};
        fi
      fi
    - docker build --tag "${TAG}"
          --build-arg "BASE_IMAGE=registry.gitlab.com/vicamo/docker-buildpack-deps/buildpack-deps:${JOB_SERIES}"
          --build-arg "KTEAMTOOLS_URL=${KTEAMTOOLS_URL}"
          ${KTEAMTOOLS_BRANCH:+--build-arg "KTEAMTOOLS_BRANCH=${KTEAMTOOLS_BRANCH}"}
          ${KTEAMTOOLS_FETCH:+--build-arg "KTEAMTOOLS_FETCH=${KTEAMTOOLS_FETCH}"}
          chroot-setup
    - docker push "${TAG}"
    - if [ "${JOB_SERIES}" = "${CHROOT_SETUP_DEFAULT_SERIES}" ]; then
        docker tag "${TAG}" "${TAG%-*}";
        docker push "${TAG%-*}";
      fi
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

chroot-setup:build:base:jammy:
  extends: .chroot-setup-build-base

.chroot-setup-build: &chroot-setup-build
  stage: build
  image: docker:git
  services:
    - docker:dind
  needs:
    - chroot-setup:build:base:jammy
  variables:
    # JOB_MAKE_CHROOT_ARGS:
    JOB_BASE_SERIES: "jammy"
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - apk update
    - apk add xz
  script:
    - |
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f3)
      export JOB_ARCH=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export JOB_FLAVOR="${JOB_SERIES}-${JOB_ARCH}"
      export ARTIFACTS_DIR="output/jobs/${CI_JOB_NAME}"
    - mkdir -p "${ARTIFACTS_DIR}"

    # Include predefined variables in Dockerfile
    - export staging_base_image="${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_BASE_SERIES}"
    - docker pull --quiet "${staging_base_image}"
    - eval "$(docker inspect -f '{{range .Config.Env}}{{println .}}{{end}}' ${staging_base_image} | sed -n '/^KTEAMTOOLS_/ { s/^/export /; p }')"

    # add --privileged to mount proc, etc.
    - docker run --rm --detach --privileged
          --volume "${CI_PROJECT_DIR}:${CI_PROJECT_DIR}"
          --workdir "${CI_PROJECT_DIR}"
          --name base
          "${staging_base_image}"
          sh -c 'while true; do sleep 1000; done'
    - docker exec base
          sh -c "if [ ${JOB_ARCH} != \$(dpkg --print-architecture) ]; then update-binfmts --enable && dpkg-reconfigure qemu-user-static; fi"
    - docker exec base apt-get update --quiet
    - docker exec base dpkg -l | grep ^ii | awk '{print $2}' > base-packages.before
    - docker exec base ./chroot-setup/make_chroot "${JOB_SERIES}" "${JOB_ARCH}" ${JOB_MAKE_CHROOT_ARGS}
    # Downloaded apt lists take up to 100MB space
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" -- sh -c 'rm -rf /var/lib/apt/lists/*_dists_*'
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" -- du -sh /var/cache/apt /var/lib/apt
    # Orphaned font package takes 100MB space
    - docker exec --workdir=/ base schroot -c "${JOB_FLAVOR}" --
          sh -c 'if dpkg-query --status fonts-noto-cjk >/dev/null; then apt-get purge --yes fonts-noto-cjk; fi'
    - docker exec base cp "/etc/schroot/chroot.d/${JOB_FLAVOR}" "${KTEAMTOOLS_CHROOTS_DIR}/${JOB_FLAVOR}"
    - docker exec base dpkg -l | grep ^ii | awk '{print $2}' > "${ARTIFACTS_DIR}"/manifest
    - diff -Nu base-packages.before "${ARTIFACTS_DIR}"/manifest
    - export TAG="${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"
    - docker cp --archive "base:${KTEAMTOOLS_CHROOTS_DIR}/${JOB_FLAVOR}/." - |
          docker import --change 'CMD ["/bin/sh"]' - "${TAG}"

    # --privileged: work-around https://github.com/moby/moby/pull/42681
    - docker run --rm
          --privileged
          "${TAG}" apt-get update

    - docker push "${TAG}"
  after_script:
    - |
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f3)
      export JOB_ARCH=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export JOB_FLAVOR="${JOB_SERIES}-${JOB_ARCH}"
      export ARTIFACTS_DIR="output/jobs/${CI_JOB_NAME}"
    - eval "$(docker inspect -f '{{range .Config.Env}}{{println .}}{{end}}' base | sed -n '/^KTEAMTOOLS_/ { s/^/export /; p }')"

    - cd "${ARTIFACTS_DIR}"
    - docker cp --archive "base:${KTEAMTOOLS_CHROOTS_DIR}/${JOB_FLAVOR}/debootstrap/debootstrap.log" . || true
    - docker cp --archive "base:${KTEAMTOOLS_CHROOTS_DIR}/${JOB_FLAVOR}/var/log/bootstrap.log" . || true
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - "output/jobs/${CI_JOB_NAME}"
    when: always
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

chroot-setup:build:trusty:amd64:
  extends: .chroot-setup-build

chroot-setup:build:trusty:i386:
  extends: .chroot-setup-build

chroot-setup:build:trusty:arm64:
  extends: .chroot-setup-build

chroot-setup:build:trusty:armhf:
  extends: .chroot-setup-build

chroot-setup:build:trusty:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:xenial:amd64:
  extends: .chroot-setup-build

chroot-setup:build:xenial:i386:
  extends: .chroot-setup-build

chroot-setup:build:xenial:arm64:
  extends: .chroot-setup-build

chroot-setup:build:xenial:armhf:
  extends: .chroot-setup-build

chroot-setup:build:xenial:powerpc:
  extends: .chroot-setup-build

chroot-setup:build:xenial:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:xenial:s390x:
  extends: .chroot-setup-build

chroot-setup:build:bionic:amd64:
  extends: .chroot-setup-build

chroot-setup:build:bionic:arm64:
  extends: .chroot-setup-build

chroot-setup:build:bionic:armhf:
  extends: .chroot-setup-build

chroot-setup:build:bionic:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:bionic:s390x:
  extends: .chroot-setup-build

chroot-setup:build:bionic:i386:
  extends: .chroot-setup-build

chroot-setup:build:focal:amd64:
  extends: .chroot-setup-build

chroot-setup:build:focal:arm64:
  extends: .chroot-setup-build

chroot-setup:build:focal:armhf:
  extends: .chroot-setup-build

chroot-setup:build:focal:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:focal:riscv64:
  extends: .chroot-setup-build

chroot-setup:build:focal:s390x:
  extends: .chroot-setup-build

chroot-setup:build:jammy:amd64:
  extends: .chroot-setup-build

chroot-setup:build:jammy:arm64:
  extends: .chroot-setup-build

chroot-setup:build:jammy:armhf:
  extends: .chroot-setup-build

chroot-setup:build:jammy:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:jammy:riscv64:
  extends: .chroot-setup-build

chroot-setup:build:jammy:s390x:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:amd64:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:arm64:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:armhf:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:riscv64:
  extends: .chroot-setup-build

chroot-setup:build:kinetic:s390x:
  extends: .chroot-setup-build

chroot-setup:build:lunar:amd64:
  extends: .chroot-setup-build

chroot-setup:build:lunar:arm64:
  extends: .chroot-setup-build

chroot-setup:build:lunar:armhf:
  extends: .chroot-setup-build

chroot-setup:build:lunar:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:lunar:riscv64:
  extends: .chroot-setup-build

chroot-setup:build:lunar:s390x:
  extends: .chroot-setup-build

chroot-setup:build:mantic:amd64:
  extends: .chroot-setup-build

chroot-setup:build:mantic:arm64:
  extends: .chroot-setup-build

chroot-setup:build:mantic:armhf:
  extends: .chroot-setup-build

chroot-setup:build:mantic:ppc64el:
  extends: .chroot-setup-build

chroot-setup:build:mantic:riscv64:
  extends: .chroot-setup-build

chroot-setup:build:mantic:s390x:
  extends: .chroot-setup-build

.chroot-setup-test: &chroot-setup-test
  stage: test
  image: docker:git
  services:
    - docker:dind
  variables:
    GIT_STRATEGY: none
    JOB_TEST_BRANCH: "master"
  before_script:
    - echo "${CI_REGISTRY_PASSWORD}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
  script:
    - |
      export JOB_BASE_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f3)
      export JOB_SERIES=$(echo "${CI_JOB_NAME}" | cut -d: -f4)
      export JOB_ARCH=$(echo "${CI_JOB_NAME}" | cut -d: -f5)
      export JOB_FLAVOR="${JOB_SERIES}-${JOB_ARCH}"

    - mkdir -p "${CI_BUILDS_DIR}/chroots/${JOB_FLAVOR}"
    - docker create --name "${JOB_FLAVOR}" "${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"
    - docker export "${JOB_FLAVOR}" | tar -C "${CI_BUILDS_DIR}/chroots/${JOB_FLAVOR}" -xpf -
    - docker rm "${JOB_FLAVOR}"
    - docker rmi "${CHROOT_SETUP_STAGING_PREFIX}${JOB_FLAVOR}"

    # Retry git-clone for it might fail with:
    #   error: RPC failed; HTTP 500 curl 22 The requested URL returned error: 500
    - |
      export JOB_REFERENCE_REPO_URL="https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/${JOB_SERIES}"
      export JOB_RUNNER_REF_MIRROR_DIR="/runner/mirrors/${JOB_REFERENCE_REPO_URL##*//}.git"
      for retry in 1 2 3; do \
        if git clone -o "${JOB_SERIES}" -b "${JOB_TEST_BRANCH}" --depth 1 \
            $(test ! -e "${JOB_RUNNER_REF_MIRROR_DIR}" || echo --reference "${JOB_RUNNER_REF_MIRROR_DIR}") \
            "${JOB_REFERENCE_REPO_URL}" "${CI_BUILDS_DIR}/${JOB_SERIES}"; then \
          break; \
        fi; \
        sleep 20; \
      done

    # Include predefined variables in Dockerfile
    - export staging_base_image="${CHROOT_SETUP_STAGING_PREFIX}base-${JOB_BASE_SERIES}"
    - docker pull --quiet "${staging_base_image}"
    - eval "$(docker inspect -f '{{range .Config.Env}}{{println .}}{{end}}' ${staging_base_image} | sed -n '/^KTEAMTOOLS_/ { s/^/export /; p }')"

    - export CONTAINER_NAME=base
    # add --privileged to mount proc, etc.
    - docker run --rm --detach --privileged --init
          --volume "${CI_BUILDS_DIR}/chroots:${KTEAMTOOLS_CHROOTS_DIR}"
          --volume "${CI_BUILDS_DIR}/${JOB_SERIES}:/home/build"
          --name "${CONTAINER_NAME}"
          "${staging_base_image}"
          /bin/sh -c "echo 'Init done'; sleep 1d"
    - echo -n "Waiting for container running"; for i in $(seq 1 20); do
        echo -n .;
        sleep 1;
        test "$(docker inspect -f '{{.State.Status}}' ${CONTAINER_NAME})" != "running" || { echo "done"; break; };
      done
    - count=0; while test ${count} -lt 10; do
        test "$(docker inspect -f '{{.State.Status}}' ${CONTAINER_NAME})" = "running" || exit 1;
        { docker logs "${CONTAINER_NAME}" 2>&1 | grep -q '^Init done'; } && break;
        sleep 3;
        count=$((count+1));
      done
    - docker exec base
          sh -c "if [ ${JOB_ARCH} != \$(dpkg --print-architecture) ]; then update-binfmts --enable && dpkg-reconfigure qemu-user-static; fi"
    - docker exec "${CONTAINER_NAME}" schroot --info --chroot="${JOB_FLAVOR}"

    - docker exec "${CONTAINER_NAME}"
          schroot --chroot="${JOB_FLAVOR}" --directory=/home/build --
              fakeroot debian/rules clean
    - docker exec "${CONTAINER_NAME}"
          schroot --chroot="${JOB_FLAVOR}" --directory=/home/build --
              fakeroot debian/rules binary-headers
    - docker exec "${CONTAINER_NAME}" ls -al /home

    - docker stop "${CONTAINER_NAME}"
  rules:
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - chroot-setup/*
        - libs/*
    - if: $CI_MERGE_REQUEST_IID == null

# chroot-setup:test:jammy:trusty

chroot-setup:test:jammy:trusty:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:trusty:amd64
      artifacts: false

chroot-setup:test:jammy:trusty:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:trusty:arm64
      artifacts: false

chroot-setup:test:jammy:trusty:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:trusty:armhf
      artifacts: false

chroot-setup:test:jammy:trusty:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:trusty:i386
      artifacts: false

chroot-setup:test:jammy:trusty:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:trusty:ppc64el
      artifacts: false

# chroot-setup:test:jammy:xenial

chroot-setup:test:jammy:xenial:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:amd64
      artifacts: false

chroot-setup:test:jammy:xenial:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:arm64
      artifacts: false

chroot-setup:test:jammy:xenial:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:armhf
      artifacts: false

chroot-setup:test:jammy:xenial:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:i386
      artifacts: false

chroot-setup:test:jammy:xenial:powerpc:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:powerpc
      artifacts: false

chroot-setup:test:jammy:xenial:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:ppc64el
      artifacts: false

chroot-setup:test:jammy:xenial:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:xenial:s390x
      artifacts: false

# chroot-setup:test:jammy:bionic

chroot-setup:test:jammy:bionic:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:amd64
      artifacts: false

chroot-setup:test:jammy:bionic:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:arm64
      artifacts: false

chroot-setup:test:jammy:bionic:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:armhf
      artifacts: false

chroot-setup:test:jammy:bionic:i386:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:i386
      artifacts: false

chroot-setup:test:jammy:bionic:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:ppc64el
      artifacts: false

chroot-setup:test:jammy:bionic:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:bionic:s390x
      artifacts: false

# chroot-setup:test:jammy:focal

chroot-setup:test:jammy:focal:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:amd64
      artifacts: false

chroot-setup:test:jammy:focal:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:arm64
      artifacts: false

chroot-setup:test:jammy:focal:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:armhf
      artifacts: false

chroot-setup:test:jammy:focal:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:ppc64el
      artifacts: false

chroot-setup:test:jammy:focal:riscv64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:riscv64
      artifacts: false

chroot-setup:test:jammy:focal:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:focal:s390x
      artifacts: false

# chroot-setup:test:jammy:jammy

chroot-setup:test:jammy:jammy:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:amd64
      artifacts: false

chroot-setup:test:jammy:jammy:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:arm64
      artifacts: false

chroot-setup:test:jammy:jammy:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:armhf
      artifacts: false

chroot-setup:test:jammy:jammy:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:ppc64el
      artifacts: false

chroot-setup:test:jammy:jammy:riscv64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:riscv64
      artifacts: false

chroot-setup:test:jammy:jammy:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:jammy:s390x
      artifacts: false

# chroot-setup:test:jammy:kinetic

chroot-setup:test:jammy:kinetic:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:amd64
      artifacts: false

chroot-setup:test:jammy:kinetic:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:arm64
      artifacts: false

chroot-setup:test:jammy:kinetic:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:armhf
      artifacts: false

chroot-setup:test:jammy:kinetic:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:ppc64el
      artifacts: false

chroot-setup:test:jammy:kinetic:riscv64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:riscv64
      artifacts: false

chroot-setup:test:jammy:kinetic:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:kinetic:s390x
      artifacts: false

# chroot-setup:test:jammy:lunar

chroot-setup:test:jammy:lunar:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:amd64
      artifacts: false

chroot-setup:test:jammy:lunar:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:arm64
      artifacts: false

chroot-setup:test:jammy:lunar:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:armhf
      artifacts: false

chroot-setup:test:jammy:lunar:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:ppc64el
      artifacts: false

chroot-setup:test:jammy:lunar:riscv64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:riscv64
      artifacts: false

chroot-setup:test:jammy:lunar:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:lunar:s390x
      artifacts: false

# chroot-setup:test:jammy:mantic

chroot-setup:test:jammy:mantic:amd64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:amd64
      artifacts: false

chroot-setup:test:jammy:mantic:arm64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:arm64
      artifacts: false

chroot-setup:test:jammy:mantic:armhf:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:armhf
      artifacts: false

chroot-setup:test:jammy:mantic:ppc64el:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:ppc64el
      artifacts: false

chroot-setup:test:jammy:mantic:riscv64:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:riscv64
      artifacts: false

chroot-setup:test:jammy:mantic:s390x:
  extends: .chroot-setup-test
  needs:
    - chroot-setup:build:base:jammy
    - job: chroot-setup:build:mantic:s390x
      artifacts: false
