#!/usr/bin/python3
from __future__ import print_function

from copy import deepcopy
from difflib import unified_diff
import json
import os
import re
import sys
import yaml
import argparse

from launchpadlib.launchpad import Launchpad

# Add ../libs to the Python search path
sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__),
                                              os.pardir, 'libs')))

from ktl.kernel_series import KernelSeries

crankers = ['/~canonical-kernel-crankers']


def generate_grant(create=False, push=False, rewind=False, person=None):
    grant = {}

    if person == 'owner':
        grant['grantee_type'] = "Repository owner"
        grant['grantee_link'] = None
    elif person:
        grant['grantee_type'] = "Person"
        grant['grantee_link'] = 'https://api.launchpad.net/devel' + person
    else:
        raise ValueError("owner or person required")

    grant['can_create'] = create
    grant['can_push'] = push
    grant['can_force_push'] = rewind

    return grant

def sort_grants(grants):
    return sorted(deepcopy(grants), key=lambda x: (x['grantee_type'], x['grantee_link']))


class RuleSet:

    def __init__(self, path=None, dry_run=False):
        self.path = path
        self.dry_run = dry_run

        self.repo_tag = set()
        self.patterns = []
        self.grants = {}
        self.subs = set()

    def add_tag(self, tag):
        if tag is None:
            raise ValueError("NONE")
        #print(self.path, tag)
        self.repo_tag.add(tag)

    def add_grant(self, pattern, grant):
        if isinstance(pattern, tuple):
            raise "WTF"
        grants = self.grants.setdefault(pattern, [])
        if grant not in grants:
            grants.append(grant)
        if pattern not in self.patterns:
            self.patterns.append(pattern)

    def add_subscriptions(self, subscriptions):
        for subscription in subscriptions:
            self.subs.add(subscription)

    def pattern_key(self, pattern):
        if '*' in pattern:
            return (1, )
        else:
            return (0, pattern)

    @property
    def rules(self):
        return [{'ref_pattern': pattern, 'grants': sort_grants(self.grants[pattern])}
            for pattern in sorted(self.patterns, key=self.pattern_key)]

    @property
    def subscriptions(self):
        return list(self.subs)

    def update(self):
        git_repo = lp.git_repositories.getByPath(path=self.path)
        if git_repo is not None:
            update_rules(git_repo, self.rules, dry_run=self.dry_run)
            update_subscriptions(git_repo, self.subscriptions, dry_run=self.dry_run)
            update_webhooks(git_repo, dry_run=self.dry_run)

    @property
    def tags(self):
        return self.repo_tag

    @property
    def subscriptions(self):
        return self.subs

    def __str__(self):
        print(self.patterns)


class RepoSet:

    def __init__(self, dry_run=False):
        self.dry_run = dry_run

        self.repos = {}

    def lookup(self, path):
        if path not in self.repos:
            self.repos[path] = RuleSet(path=path, dry_run=self.dry_run)
        return self.repos[path]

    def __str__(self):
        result = []
        for repo_path, repo_rules in sorted(self.repos.items()):
            result.append("{}: {}".format(repo_path, repo_rules.tags))
        return '\n'.join(result)


def update_rules(git_repo, rules, show=False, dry_run=False):
    # Get and sort the rules so we can compare against them.
    current_rules = git_repo.getRules()
    for rule in current_rules:
        rule['grants'] = sort_grants(rule['grants'])

    if show is True:
        print(" is:")
        rule_txt = yaml.dump(current_rules, default_flow_style=False)
        for line in rule_txt.split('\n'):
            print("  " + line)

    current_rules_cmp = yaml.dump(sorted(current_rules, key=lambda x: x['ref_pattern']), default_flow_style=False)
    rules_cmp = yaml.dump(sorted(rules, key=lambda x: x['ref_pattern']), default_flow_style=False)

    if current_rules != rules and current_rules_cmp == rules_cmp:
        print(" different _but_ semantically the same")
    elif current_rules != rules:
        rule_is = yaml.dump(current_rules, default_flow_style=False)
        rule_want = yaml.dump(rules, default_flow_style=False)
        bits_is = rule_is.split('\n')
        bits_want = rule_want.split('\n')
        diff = unified_diff(bits_is, bits_want, lineterm="")
        print("  " + "\n  ".join(diff))
        if dry_run:
            print(" dry run, rules not updated")
        else:
            print(" rules updated")
            git_repo.setRules(rules=rules)

def update_subscriptions(git_repo, crankers, dry_run=False):
    if not git_repo.private:
        return

    # For private repositories we need to add an empty subscription
    # for each cranker.  Look up all the existing subscriptions.
    subscriber_seen = set()
    for subscription in git_repo.subscriptions:
        #print(subscription, subscription.person)
        subscriber_seen.add(subscription.person.name)

    for cranker in crankers:
        cranker = cranker[2:]
        if cranker not in subscriber_seen:
            person = lp.people(cranker)
            print(" cranker:")
            print("  " + cranker)
            if dry_run:
                print(" dry run, subscriptions not updated")
            else:
                git_repo.subscribe(person=person,
                    code_review_level='No email',
                    max_diff_lines="Don't send diffs",
                    notification_level='No email')

webhook_url = 'http://10.15.182.10/webhooks'
webhook_events = ['git:push:0.1']
#def update_webhooks(git_repo, dry_run=False):
#    live_hook = None
#    for hook in git_repo.webhooks:
#        print(hook, hook.delivery_url, hook.event_types, hook.active)
#        if hook.delivery_url == webhook_url:
#            live_hook = hook
#            break
#    else:
#        live_hook = git_repo.newWebhook(delivery_url=webhook_url, event_types=webhook_events, active=True)
#
#    update_hook = False
#    if live_hook.active != True:
#        live_hook.active = True
#        update_hook = True
#    if live_hook.event_types != webhook_events:
#        live_hook.event_types = webhook_events
#        update_hook = True
#    if update_hook:
#        if dry_run:
#            print(" dry run, webhooks not updated")
#        else:
#            live_hook.lp_save()

def update_webhooks(git_repo, dry_run=False):
    live_hook = None
    for hook in git_repo.webhooks:
        print(hook, hook.delivery_url, hook.event_types, hook.active)
        if hook.delivery_url == webhook_url:
            live_hook = hook
            break

    if live_hook is not None:
        if dry_run:
            print(" dry run, webhooks not removed")
        else:
            live_hook.lp_delete()

def list_all(args):
    me = lp.people(args.user)
    for repo in lp.git_repositories.getRepositories(target=me):
        print(repo)
        current_rules = repo.getRules()
        rule_txt = yaml.dump(current_rules, default_flow_style=False)
        for line in rule_txt.split('\n'):
            print("  " + line)
        sys.stdout.flush()

def sync_tagged(repo_set, tag):
    for repo in sorted(repo_set.repos):
        repo_rules = repo_set.lookup(repo)
        if tag in repo_rules.tags:
            print(repo, repo_rules.tags)
            repo_rules.update()

def add_misc(repo_set):

    admins_core = [
        '/~canonical-kernel-repo-admins-active',
    ]
    admins_test = [
        '/~canonical-kernel-test-admins',
    ]
    people_core = [
        "/~canonical-kernel-committers",
        "/~canonical-kernel-hwe-committers",
    ]
    people_tools = [
        "/~canonical-kernel-tools-committers",
    ]

    series = [x for x in kernel_series.series if x.supported or x.development]
    fw_branches = [(x.codename, x.development) for x in series]

    # Repo branches is a list of (<branch>, <rewind>) pairs.  Each branch
    # will have an acl created and if rewind is true that will allow force
    # push.
    for repo_path, admins, people, repo_branches, tags_wild in [
        [
            "~canonical-kernel/+git/kteam-tools",
            admins_core,
            people_core + people_tools,
            [("master", False)],
            None,
        ],
        [
            "~canonical-kernel/+git/adt-matrix-hints",
            admins_core,
            people_core + people_tools,
            [("master", False)],
            None,
        ],
        [
            "~canonical-kernel/+git/kernel-versions",
            admins_core,
            people_core + people_tools,
            [("*", True)],
            None,
        ],
        [
            "~canonical-kernel/+git/cbd",
            admins_core,
            people_core + people_tools,
            [("*", True)],
            None,
        ],
        [
            "~ubuntu-kernel/ubuntu/+source/linux-firmware",
            admins_core,
            people_core + people_tools,
            [("master", True)] + fw_branches,
            "Ubuntu-*",
        ],
        [
            "~canonical-kernel/+git/ksc-test-results",
            admins_core + admins_test,
            people_core + people_tools,
            [("main", False)],
            None,
        ],
    ]:

        print("misc: {}".format(os.path.basename(repo_path)))

        repo_rules = repo_set.lookup(repo_path)

        repo_rules.add_subscriptions(crankers)
        repo_rules.add_tag(os.path.basename(repo_path))
        repo_rules.add_tag('misc')

        # Note: if we are allowing a user to rewind branches we will also grant create.
        for branch, allow_rewind in repo_branches:
            ref = "refs/heads/{}".format(branch)
            for person in people:
                repo_rules.add_grant(ref, generate_grant(person=person, create=allow_rewind, push=True, rewind=allow_rewind))
            for admin in admins:
                repo_rules.add_grant(ref, generate_grant(person=admin, create=True, push=True, rewind=True))
            repo_rules.add_grant(ref, generate_grant(person="/~ubuntu-kernel-bot", create=True, push=True, rewind=True))
            if admins != 'owner':
                repo_rules.add_grant(ref, generate_grant(person='owner'))

        if tags_wild is not None:
            ref = "refs/tags/{}".format(tags_wild)

            for person in people:
                repo_rules.add_grant(ref, generate_grant(person=person, create=True))
            for admin in admins:
                repo_rules.add_grant(ref, generate_grant(person=admin, create=True, push=True, rewind=True))
            if 'owner' not in admins:
                repo_rules.add_grant(ref, generate_grant(person='owner'))

def sync_misc(args):
    sync_tagged(args.repo_set, 'misc')

def add_esm(repo_set):
    admins = '/~canonical-kernel-repo-admins-active'

    in_esm = set()
    for series in sorted(kernel_series.series, key=KernelSeries.key_series_name):
        if not series.esm:
            continue
        in_esm.add(series.codename)

    for team_name in ('ubuntu-kernel', 'canonical-kernel'):
        team = lp.people(team_name)
        for git_repo in lp.git_repositories.getRepositories(target=team):
            bits = git_repo.self_link.split('/')
            series = bits[-1]
            if bits[-2] != '+git' or series not in in_esm:
                continue
            print("esm: {}".format(git_repo.unique_name))
            #print(git_repo, 'ESM', git_repo.unique_name)
            repo_rules = repo_set.lookup(git_repo.unique_name)
            repo_rules.add_tag("esm")
            repo_rules.add_tag(series)
            repo_rules.add_grant('*', generate_grant(person=admins, create=True, push=True, rewind=True))
            if admins != 'owner':
                repo_rules.add_grant('*', generate_grant(person='owner'))
            repo_rules.add_subscriptions(crankers)
            #update_webhooks(git_repo, dry_run=args.dry_run)

def sync_esm(args):
    sync_tagged(args.repo_set, 'esm')

# XXX: we need a better way to control this.
def is_hwe(source):
    return source.name.startswith('linux-oem') or source.name in (
            'linux-cascade', 'linux-bluefield', 'linux-denver', 'linux-fde',
            'linux-intel', 'linux-iot')

def is_nvidia(source):
    return source.name.startswith('linux-nvidia')

def add_primary(repo_set):
    admins = '/~canonical-kernel-repo-admins-active'

    repo_things = {}

    for series in sorted(kernel_series.series, key=KernelSeries.key_series_name):
        #if series.codename != 'bionic':
        #    continue
        if not args.initialise_development and series.opening_ready('repositories') is False:
            print("IGNORE: {} series not ready".format(series.codename))
            continue

        for source in sorted(series.sources, key=lambda x: x.name):
            #if series.codename != 'bionic' or source.name != 'linux':
            #    continue
            #if series.codename != 'focal':
            #    continue
            #if source.name != 'linux-denver':
            #    continue

            #print("APW", source, source.packages, source.snaps)
            for thing in source.packages + source.snaps:
                #print("APW", thing)
                if thing.repo is None:
                    continue
                url = thing.repo.url

                if 'git.launchpad.net' not in url:
                    continue
                if 'tillamook' in url:
                    continue

                bits = url.split('~', 1)
                if len(bits) != 2:
                    continue

                path = '~' + bits[1]

                repo_things.setdefault(path, []).append(thing)
                #branch = package.repo.branch
                #repo_packages.setdefault(path, []).append(branch)
                #if branch.endswith('-next'):
                #    repo_packages.setdefault(path).append(branch[:-5])

    snap_branch_prefix_re = re.compile('^(.*?)(-[0-9\.]+)?$')

    for path, path_things in repo_things.items():
        repo_rules = repo_set.lookup(path)

        version_people = dict()
        any_supported = False
        snap_master_added = False
        snap_prefix_added = {}
        for thing in path_things:
            print("primary: {}:{}".format(thing.source.series.codename, thing.source.name))
            #print(thing, thing.source.development, thing.source.supported)
            thing_type = getattr(thing, 'type', 'snap')
            if thing_type is None:
                thing_type = 'main'

            repo_rules.add_tag(thing_type)
            repo_rules.add_tag(thing.series.codename)
            repo_rules.add_tag("{}:{}".format(thing.series.codename, thing.source.name))

            if not is_hwe(thing.source) and thing.source.development is False and thing.source.supported is False:
                continue
            #if thing.series.supported is False:
            #    continue

            any_supported = True
            #print(thing, thing.source.development, thing.source.supported)

            thing_type = getattr(thing, 'type', 'snap')
            if thing_type is None:
                thing_type = 'main'

            #repo_rules.add_tag(thing_type)
            #repo_rules.add_tag(thing.series.codename)
            repo_rules.add_subscriptions(crankers)

            people = ['/~canonical-kernel-committers']
            if is_hwe(thing.source):
                people.append('/~canonical-kernel-hwe-committers')
            if is_nvidia(thing.source):
                people.append('/~nvidia-kernel-committers')
                repo_rules.add_subscriptions(['/~nvidia-kernel-team'])
            #admins = 'owner'
            #if series.esm or package.name.endswith('ibm-gt'):
            #    admins = '/~canonical-kernel-repo-admins'
            # XXX: until we drop people from the ubuntu-kernel and canonical-kernel
            #      we need to bodge their admin-ness away.

            branch = thing.repo.branch
            branches = []
            # Order here matters to launchpad, see LP: #1815431.
            if branch.endswith('-next'):
                branches.append(branch[:-5])
            #print(thing, branch)
            branches.append(branch)
            # Inclusive naming migration rules
            if branch == 'master-next':
                branches.append('main-next')
                branches.append('main')
            if branch == 'master':
                branches.append('main')
            # snap repositories should also have a -test branch.
            if thing_type == 'snap':
                branches.append(branch + '-test')
            # snap repositories have a fungible master shared by the remainder.
            if thing_type == 'snap' and not snap_master_added:
                snap_master_added = True
                branches.insert(0, 'master')
                branches.insert(0, 'master-test')
                branches.insert(0, 'main')
                branches.insert(0, 'main-test')
            # debs repositories have a -prep branch.
            if thing_type in (None, 'main'):
                branches.append(branches[0] + '-prep')
            for branch_name in branches:
                if thing.series.development:
                    rewind = True
                elif thing.source.derived_from is not None and thing_type in (None, 'main'):
                    rewind = True
                elif branch_name.endswith('-next') or branch_name.endswith('-prep'):
                    rewind = True
                else:
                    rewind = False
                ref = "refs/heads/{}".format(branch_name)
                for person in people:
                    repo_rules.add_grant(ref, generate_grant(person=person, create=True, push=True, rewind=rewind))

            # TAGS: allow Ubuntu-<branch>- prefixed tags.
            if thing_type == 'snap':
                # Drop any numerical suffix from the branch name.
                match = snap_branch_prefix_re.search(branch)
                if match:
                    branch_prefix = '-' + match.group(1)
                else:
                    branch_prefix = '-' + branch
                repo_rules.add_tag("snap")

            else:
                branch_prefix = thing.source.name.replace('linux', '')
                repo_rules.add_tag("primary")
            branch_prefixes = [ branch_prefix ]
            if branch_prefix.startswith('-pc'):
                branch_prefixes.append(branch_prefix[3:])
            elif branch_prefix.startswith('-lts-'):
                branch_prefixes.append('-lts')
            rewind_tags = thing.series.development
            for branch_prefix in branch_prefixes:
                ref = "refs/tags/Ubuntu{}-[0-9]*".format(branch_prefix)
                if branch_prefix in snap_prefix_added:
                    continue
                snap_prefix_added[branch_prefix] = True
                for person in people:
                    repo_rules.add_grant(ref, generate_grant(person=person, create=True, rewind=rewind_tags))

            # TAGS: for main packages allow version tags to be pushed too.
            if thing_type in (None, 'main'):
                pkg_versions = thing.source.versions
                if pkg_versions is not None:
                    for version in pkg_versions:
                        for person in people:
                            version_people.setdefault(version, set()).add(person)

        if not any_supported:
            repo_rules.add_tag("eol")

        # Add the administrator rules.
        series = path_things[0].series
        repo_rules.add_grant('*', generate_grant(person=admins, create=True, push=True, rewind=True))
        if admins != 'owner':
            repo_rules.add_grant('*', generate_grant(person='owner'))

        # Add versioned tag rules.
        for version, people in version_people.items():
            if version.endswith('.0'):
                version = version[:-2]
            ref = "refs/tags/v{}*".format(version)
            for person in people:
                repo_rules.add_grant(ref, generate_grant(person=person, create=True, push=True, rewind=rewind_tags))
                repo_rules.add_grant(ref, generate_grant(person=admins, create=True, push=True, rewind=True))
                if admins != 'owner':
                    repo_rules.add_grant(ref, generate_grant(person='owner'))

def sync_primary(args):
    sync_tagged(args.repo_set, 'primary')

def sync_snap(args):
    sync_tagged(args.repo_set, 'snap')

def sync(args):
    sync_tagged(args.repo_set, args.tag)

def sync_all(args):
    print("SYNC: primary")
    sync_primary(args)
    print("SYNC: snap")
    sync_snap(args)
    print("SYNC: esm")
    sync_esm(args)
    print("SYNC: misc")
    sync_misc(args)

desc = "List or update permissions for kernel team repositories in Launchpad."
parser = argparse.ArgumentParser(description=desc)
parser.add_argument("-d", "--dry-run", action="store_true", default=False,
                    help="do not make any changes, just show what would be done")
parser.add_argument("--initialise-development", action="store_true", default=False,
                    help="override opening: repositories tag to allow initialisation")

subparsers = parser.add_subparsers(help="commands", dest="command", required=True)

subparser = subparsers.add_parser("list-all", help="list permissions for users")
subparser.add_argument("user", help="launchpad user")
subparser.set_defaults(func=list_all)

subparser = subparsers.add_parser("sync-misc", help="update misc repository permissions")
subparser.set_defaults(func=sync_misc)

subparser = subparsers.add_parser("sync-esm", help="update ESM repository permissions")
subparser.set_defaults(func=sync_esm)

subparser = subparsers.add_parser("sync-primary", help="update primary repository permissions")
subparser.set_defaults(func=sync_primary)

subparser = subparsers.add_parser("sync-snap", help="update snap repository permissions")
subparser.set_defaults(func=sync_snap)

subparser = subparsers.add_parser("sync", help="update repository permissions by tag")
subparser.set_defaults(func=sync)
subparser.add_argument("tag")

subparser = subparsers.add_parser("sync-all", help="update all repository permissions")
subparser.set_defaults(func=sync_all)

args = parser.parse_args()

lp = Launchpad.login_with('admin-lp-git-permissions', 'production', version='devel')

repo_set = RepoSet(dry_run=args.dry_run)

kernel_series = KernelSeries()

add_primary(repo_set)
add_esm(repo_set)
add_misc(repo_set)

args.repo_set = repo_set

#print(repo_set)
#for repo in sorted(repo_set.repos):
#   print(repo, repo_set.lookup(repo).tags)

#repo_rules = repo_set.lookup("~ubuntu-kernel/ubuntu/+source/linux-snap/+git/xenial")
#print(repo_rules.rules())

args.func(args)
